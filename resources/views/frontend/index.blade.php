@extends('frontend.master')
@section('main')
<style>
        .vieni {
            border: 10px solid #4267b2 !important;
            color: #4267b2 !important;
            font-size: 40px;
            font-weight: bolder !important;
            padding-top: 0px !important;
        }
        .title small{
            color:#777;
            font-size:14px;
            line-height:24px;
            text-align:center;
            font-family: 'Roboto Slab', serif;
        }
        .tdtintuchome p{
            color:#777;
             font-size:14px;
            line-height:24px;
            text-align:center;
            font-family: 'Roboto Slab', serif;
            
        }
</style>

 	<div class="badichvu badichvuabout" style="background: #F6F7F9">
        <div class="container"  style="margin-bottom: -20px;">
            <div class="row">
                <div class="col-sm-12 text-xs-center  dichvu">
                   
                       <h2 class="roboto">DỊCH VỤ MỞ KHÓA FACE BOOK</h2>
                       <p class="roboto2" >Cách đơn giản nhất là hãy kết nối với chúng tôi</p>
                 
                </div>
                <div class="col-sm-3 wow flipInY text-xs-center" style="visibility: visible; animation-name: flipInY;">
                    <a href="#" class="vieni" style=""><i class="fa flaticon-home-1 fa-lock icon"></i></a>
                    <h3><a href="#">FAQ 030 (vi phạm chính sách của Facebook)</a></h3>
                   
                    
                </div>
                <div class="col-sm-3 wow flipInY text-xs-center" style="visibility: visible; animation-name: flipInY;">
                    <a href="#" class="vieni" style=""><i class="fa flaticon-home-1 fa-lock icon"></i></a>
                    <h3><a href="#">Checkpoint 72h (úp ảnh khuôn mặt)</a></h3>
                   
                    
                </div>
                <div class="col-sm-3 wow flipInY text-xs-center" style="visibility: visible; animation-name: flipInY;">
                    <a href="#" class="vieni" style=""><i class="fa flaticon-home-1 fa-lock icon"></i></a>
                    <h3><a href="#">FAQ 277 (bị khóa giả mạo)</a></h3>
                    
                    
                </div>
                <div class="col-sm-3 wow flipInY text-xs-center" style="visibility: visible; animation-name: flipInY;">
                    <a href="#" class="vieni" style=""><i class="fa flaticon-home-1 fa-lock icon"></i></a>
                    <h3><a href="#">FAQ 583 (bị khóa mạo danh, bắt nạt, quấy rối)</a></h3>
                   
                    
                </div>	
                		
            </div> <!-- het row -->
        </div> <!--  het container -->
    </div> <!--  het 3dichvu-->


    <div class="badichvu badichvuabout" >
            <div class="container " style="margin-bottom: 50px;">
                <div class="row qt" >
                    <div class="col-sm-12 text-xs-center  dichvu1">
                   
                        <h2 class="roboto">QUY TRÌNH MỞ KHÓA FACEBOOK</h2>
                       
                  
                 </div>
                    <div class="col-sm-3 wow flipInY text-xs-center" style="visibility: visible; animation-name: flipInY;">
                        <a href="#" class="vieni"><p style=" color: #4267b2 !important;
                        font-size: 40px;
                        font-weight: bolder !important;padding-top:25px;">1</p></a>
                        <h3><a href="#">Khách hàng cung cấp thông tin tài khoản.</a></h3>
                        
                        
                    </div>
                    <div class="col-sm-3 wow flipInY text-xs-center" style="visibility: visible; animation-name: flipInY;">
                            <a href="#" class="vieni" ><p style=" color: #4267b2 !important;
                            font-size: 40px;
                            font-weight: bolder !important;padding-top:25px;">2</p></a>
                            <h3><a href="#">Tiếp nhận, kiểm tra, báo giá và nêu hướng xử lí cho khách hàng.</a></h3>
                          
                            
                    </div>
                    <div class="col-sm-3 wow flipInY text-xs-center" style="visibility: visible; animation-name: flipInY;">
                            <a href="#" class="vieni"><p style=" color: #4267b2 !important;
                            font-size: 40px;
                            font-weight: bolder !important;padding-top:25px;">3</p> </a>
                            <h3><a href="#">Thực hiện mở khóa trong vòng 24h kể từ khi tiếp nhận.</a></h3>
                            
                            
                    </div>
                    <div class="col-sm-3 wow flipInY text-xs-center" style="visibility: visible; animation-name: flipInY;">
                            <a href="#" class="vieni" ><p style=" color: #4267b2 !important;
                            font-size: 40px;
                            font-weight: bolder !important;padding-top:25px;">4</p></a>
                            <h3><a href="">Thông báo tình trạng, nếu hoàn thành thì khách hàng thanh toán.</a></h3>
                            
                            
                    </div>
                   	
                </div> <!-- het row -->
            </div> <!--  het container -->
    </div> <!--  het 3dichvu-->




        <div class="container khcan">
            <div class="row "> 
                    <div class="col-sm-12 text-xs-center dichvu2">
                               <h2 class="roboto">BẢNG GIÁ</h2>  
                             
                    </div>
            </div>
            <div class="container " style="">
                        @foreach ($banggia as $banggia)
                        <div class="row">
                                <div class="col-sm-12 text-xs-center">
                                        <img src="{{ asset('storage/app/img/'.$banggia->img) }}" alt="" class="img-fluid" style="height:300px;width:800px;" >
                                </div>
                            </div>
                        @endforeach          
            </div>
        </div> <!--het khach hang can-->
        
        <div class="row "> 
            <div class="col-sm-12 text-xs-center dichvu3 ">
                       <h2 class="roboto">KHÁCH HÀNG CỦA CHÚNG TÔI</h2>  
                       <small class="roboto2">Bạn đang là cá nhân, kinh doanh online, công ty, ca sĩ, doanh nghiệp,... Bạn đang cần tìm đối tác hợp tác lâu dài? Hãy đến với chúng tôi ngay hôm nay.</small>
            </div>
           
        </div>
        <div class=" khachhang wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                <div class="container">
       
                    <div class="row">
                    <div class="col-sm-12">
                                <div id="slidemonanduoi" data-interval="3000" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        
                                        <li data-target="#slidemonanduoi" data-slide-to="1" class=""></li>
                                        <li data-target="#slidemonanduoi" data-slide-to="2" class=""></li>
                                    </ol>
                                    <div class="carousel-inner" role="listbox">
                                        <div class="carousel-item active">
                                            <div class="row">
                                                <div class="sanpham">
                                                    <img src="images/kh6.png" alt="" class="anhspslide">
                                                   
                                                </div> <!-- SAN PHAM -->
       
                                                <div class="sanpham">
                                                    <img src="images/kh8.png" alt="" class="anhspslide">
                                                    
                                                </div> <!-- SAN PHAM -->
       
                                                <div class="sanpham">
                                                    <img src="images/kh7.png" alt="" class="anhspslide">
                                                    
                                                </div> <!-- SAN PHAM -->
       
                                                <div class="sanpham">
                                                    <img src="images/kh4.png" alt="" class="anhspslide">
                                                    
                                                </div> <!-- SAN PHAM -->
       
                                                <div class="sanpham">
                                                    <img src="images/kh5.png" alt="" class="anhspslide">
                                                    
                                                </div> <!-- SAN PHAM -->
       
                                                
                                            </div> <!-- het row -->
       
                                        </div>  <!-- HET CAROUSEL ITEM -->
                                         <div class="carousel-item">
                                            <div class="row">
                                                <div class="sanpham">
                                                    <img src="images/kh6.png" alt="" class="anhspslide">
                                                   
                                                </div> <!-- SAN PHAM -->
       
                                                <div class="sanpham">
                                                    <img src="images/kh7.png" alt="" class="anhspslide">
                                                   
                                                </div> <!-- SAN PHAM -->
       
                                                <div class="sanpham">
                                                    <img src="images/kh8.png" alt="" class="anhspslide">
                                                    
                                                </div> <!-- SAN PHAM -->
       
                                                <div class="sanpham">
                                                    <img src="images/kh9.png" alt="" class="anhspslide">
                                                   
                                                </div> <!-- SAN PHAM -->
       
                                                <div class="sanpham">
                                                    <img src="images/kh10.png" alt="" class="anhspslide">
                                                   
                                                </div> <!-- SAN PHAM -->
       
                                                
                                            </div> <!-- het row -->
       
                                        </div>  <!-- HET CAROUSEL ITEM -->  
                                    </div>
                                    
                                </div>
                        </div> <!-- het colsm12 cu monan -->
                    </div>  <!-- HET ROW -->
                </div> <!-- HET CONTAINER -->
        </div><!-- HET khachhang -->


        <div class="phanhoinguoidung khung wow  fadeInUp" data-wow-delay="0s" style="visibility: visible; animation-delay: 0s; animation-name: fadeInUp;margin-top:-20px;">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-10 push-sm-1 text-xs-center">
                            <div id="slidetestimnial" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                        @foreach ($danhgia as $key=>$item)
                                        <li data-target="#slidetestimnial" data-slide-to="{{ $key }}" class="{{ $key == 0 ? 'active' : '' }}"></li>
                                        @endforeach 
                                   
                                </ol>
                                <div class="carousel-inner " role="listbox">
                                  
                                        @foreach ($danhgia as $key=>$item) 
                                        <div class="carousel-item {{ $key == 0 ? 'active' : '' }}  ">
                                                <img src="{{ asset('storage/app/img/'.$item->img) }}" alt="" style="border: 5px solid #ffffff;width:150px;height:150px; border-radius:100px;">
                                            <div class="quote">
                                                {!! html_entity_decode($item->noidung ) !!}
                                            </div>
                                            <b class="fontdancing tennguoi"> {{ $item->name }} </b>
                                        </div> 
                                    @endforeach 
                                   
                                </div>
                                 
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!--hết fb người dùng-->


            <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-xs-center wow  flipInY" data-wow-delay="0s" style="visibility: visible; animation-delay: 0s; animation-name: flipInY;">
                            <div class="tdtintuchome">
                                <span class="utmavo" style="color: goldenrod">mokhoa247</span>
                                <p class="roboto2">Đơn giản hóa việc online của bạn</p>
                            </div>
                        </div>
        
                        <div class="col-md-4 col-sm-6 col-xs-12 wow  flipInY" data-wow-delay="0s" style="visibility: visible; animation-delay: 0s; animation-name: flipInY;">
    
                                <div class="mottinchuan text-xs-center" >
                                    <img src="images/20000.png"  style="width: 80px;" alt="">
                                    <a href="" class="tieudetin1 fontoswarld"  style="color:darkorange;font-size: 30px">+ 20000</a>
                                    
                                    <p class="fontroboto">Hơn 20000 người đã hợp tác tại dichvuFB </p>
                                </div>
                        </div> 
                        <div class="col-md-4 col-sm-6 col-xs-12 wow  flipInY" data-wow-delay="0s" style="visibility: visible; animation-delay: 0s; animation-name: flipInY;">
    
                                <div class="mottinchuan text-xs-center" >
                                    <img src="images/5.png"  style="width: 80px;" alt="">
                                    <a href="" class="tieudetin1 fontoswarld"  style="color:darkorange;font-size: 30px">+ 2000</a>
                                    
                                    <p class="fontroboto">Hơn 5 năm xây dựng và phát triển </p>
                                </div>
                        </div> 
                        <div class="col-md-4 col-sm-6 col-xs-12 wow  flipInY" data-wow-delay="0s" style="visibility: visible; animation-delay: 0s; animation-name: flipInY;">
    
                                <div class="mottinchuan text-xs-center" >
                                    <img src="images/5000.png"  style="width: 80px;" alt="">
                                    <a href="" class="tieudetin1 fontoswarld"  style="color:darkorange;font-size: 30px">+ 5000</a>
                                    
                                    <p class="fontroboto">Hơn 5000 giao dịch thành công </p>
                                </div>
                        </div> 
        
                    </div>
                </div><!-- hết phần mokhoa -->
        

@stop