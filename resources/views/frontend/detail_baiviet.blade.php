@extends('frontend.master')
@section('main')
<style>
		.p-3{
			padding: 0.5rem!important;
		}
		.border{
			border: 2px solid #dee2e6!important;
			border-radius: 5px;
		}
		.anh{
			margin-right: 10px;
		}
		.mt-3{
			margin-bottom: 10px;
		}
		.tieude{
			border-bottom: 4px solid silver;
		}
		.tieude h4{
			margin-bottom: -5px;
			background: white;
			display: inline-block;
			position: relative;
			top: 15px;
			left: 30px;
			padding-left: 15px;
			padding-right: 10px;
		}
		.noidungtin{
			background: #f7f7f7;
			padding-top: 20px;
			padding-bottom: 20px;
		}
		.tin{
			background: white;
			padding: 20px 10px 0px 30px; 
			border-radius: 10px;
		}
		.tieudetin1{
			font-size: 32px;
			color: black;
			line-height: 1.2;
			font-weight: bolder;
		}
		
		.entry-view{
			padding-right: 15px;
			padding-left:5px;
			font-size: 14px;
			color: #9f9f9f;
		}
		.flot-container {
			box-sizing: border-box;
			width: 100%;
			height: 275px;
			padding: 20px 15px 15px;
			margin: 15px auto 30px;
			background: transparent;
		}

	   
	</style>

		<section class="noidungtin ">
			<div class="container tin">
				<div class="row">
					<div class="col-sm-12">
						<div class="mottinchuan mb-3  wow  fadeInUp fontroboto">
						<hr>
						
							<h2 class="tieudetin1">{{ $baiviet->title }}</h2>
							<div class="entry-meta">
									<span class="entry-view"><i class="fa fa-eye"> 126 Views</i>  </span>
									<time class="entry-view " ><i class=" fa fa-pencil-square-o"> by Potential-team</i></time>
							</div>
							
							<p >{!! html_entity_decode($baiviet->noidung) !!} </p>
							
						</div>
						

					</div> <!-- HET COT 12-->
					
				</div>
				<div class="container">
					<div id="fb-root"></div>
						<script>(function(d, s, id) {
						var js, fjs = d.getElementsByTagName(s)[0];
						if (d.getElementById(id)) return;
						js = d.createElement(s); js.id = id;
						js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2';
						fjs.parentNode.insertBefore(js, fjs);
						}(document, 'script', 'facebook-jssdk'));</script>
					<div class="fb-comments" data-href="https://mokhoa247.com/" data-numposts="5"></div>
				</div>


				<div class="row">
					
						<div class="container">
								<div class="tieude">
									<h4>Bạn có thể quan tâm</h4>
								</div>
						</div>
							
						<div class="container mt-3 wow  fadeInUp " >
								<div class="row">
									@foreach ($lienquan as $lienquan)
										<div class="media border p-3 col-sm-4" >
											<img src="{{ asset('storage/app/img/'.$lienquan->img) }}" alt="" class="anh rounded-circle" style="width:100px;height:100px;">
											<div class="media-body">
												<a href="{{ route('postDetail', $lienquan->slug) }}"><p class="text-muted" style="margin-bottom: .5rem;
													font-size:20px;
													font-weight: 500;
													line-height: 1.2;
													padding-top:20px;
												">{{ $lienquan->title }} </p></a>
												<div class="entry-meta">
													
														<time class="entry-view " ><i class="fa fa-clock-o"> {{ $baiviet->created_at }}</i></time>
												</div>
											
											</div>
										</div>
									@endforeach
								</div>
									
					</div>
						
	
				</div><!-- HET COT 4 -->
			</div>
			

			
								
		</section><!--  het noidung tin -->
		
		
		



		
		
		

@stop