<!DOCTYPE html>
<html lang="en"><head>
	<title> Dịch Vụ Facebook Chất Lượng  </title>
    <meta charset="utf-8">
    <base href="{{ asset('public/frontend') }}">
    <meta name="description" content="Dịch vụ mở khóa tài khoản facebook uy tín, mở khóa tất cả các dạng, mokhoa247 làm xong thanh toán" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="images/favico.ico"/>  
	<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700&amp;subset=vietnamese" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
	<script type="text/javascript" src="vendor/bootstrap.js"></script>
 	<script type="text/javascript" src="vendor/isotope.pkgd.min.js"></script>
 	<script type="text/javascript" src="vendor/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="style/1.js"></script>
	<link rel="stylesheet" href="vendor/bootstrap.css">
	<link rel="stylesheet" href="vendor/font-awesome.css">
	<link rel="stylesheet" href="css/1.css">
 </head>
  
 
<body >
 	<div class="topheader " style="background: #191960">
 		<div class="container ">
 			<div class="row" >
 				<div class="col-sm-6 wow jello" style="border-bottom:1px solid white;">
 					
						<div class="datban1 openingtop float-sm-left text-sm-left text-xs-center">
                                <i class="fa fa-phone"> 0868.999.179</i>
 					    </div>
 					    <div class="datban1 t2">
                            <i class="fa fa-envelope"> </i>
                       
 					    </div>
 				</div>
 				<div class="col-sm-6  wow jello" style="border-bottom:1px solid white;">
                    <div class="mangxh float-sm-right text-xs-center text-sm-right wow jello">
                        <a class="la" href="https://www.facebook.com/"  target= "_blank" ><i class=" fa fa-facebook"></i></a>
					    <i class="la fa fa-twitter"></i>
						<i class="la fa fa-pinterest"></i>
                        <i class="la fa fa-google-plus"></i>
                       
 					</div>
                </div>
                 
 			</div> <!-- het row -->
 			
 		</div> <!-- het container -->
     </div> <!-- het topheader  -->
      
    
     
 	<div class="logovamenu  " style="background: #191960; ">
	    <nav class="navbar navbar-light  fontroboto">
	    	<div class="container">    	
			      <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#mtren"></button>
			      <div class="collapse navbar-toggleable-xs" id="mtren">
                    <a class="navbar-brand text-xs-center text-sm-left  wow fadeInUp" data-wow-delay="0.2s"" href="{{ route('home') }}"> <img src="images/logofb1.PNG"></a>
                        <ul class="nav navbar-nav len1 float-sm-right wow fadeInUp" >
                            @foreach ($cate as $cate)
                                <li class="nav-item len">
                                    <a class="nav-link" style="color: #F6F7F9; font-size:15px; " href="{{ route('postDetail',$cate->cate_slug) }}">{{ $cate->cate_name }}  </a>
                                </li>
                            @endforeach
                        </ul>
			      </div>
	      </div> <!-- het container -->
	    </nav>
     </div> <!-- het logo va menu -->
     
     <div class="slide">
        <div id="slidehome" class="carousel slide slidecon" data-ride="carousel">
                <ol class="carousel-indicators">
                    @foreach ($slide as $key=>$item)
                    <li data-target="#slidehome" data-slide-to="{{ $key }}" class="{{ $key == 0 ? 'active' : '' }}"></li>
                    @endforeach 
    
                </ol>
                <div class="carousel-inner" role="listbox">
    
                    @foreach ($slide as $key=>$item)
                        <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                            <img src="{{ asset('storage/app/img/'.$item->img) }}" alt="img-fluid" style="width:100%;">
                        </div>
                    @endforeach           
                </div>
                <a class="left carousel-control" href="#slidehome" role="button" data-slide="prev">
                    <span class="icon-prev" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#slidehome" role="button" data-slide="next">
                    <span class="icon-next" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
        </div>
      
    </div> <!-- het slide  -->
 	
     @yield('main')
        

	<div class="footertop">
		<div class="container">
			<div class="row">
                @foreach ($footer as $footer)
                    
                
				<div class="col-sm-3 cotf1 mb-2 wow fadeInUp" data-wow-delay="0s">
                        <span class="fontdancing" style="color: darkorange">mokhoa247</span>
					<p>Luôn đặt khách hàng và sự chất lượng lên hàng đầu. Cam kết làm hài lòng và dành được sự tin tưởng của khách hàng</p>
					<div class="motdong">
						<i class="fa fa-paper-plane-o"></i>
						<span class="textmd">Address : {{ $footer->diachi }}<br>
						</span>
					</div>
					<div class="motdong">
						<i class="fa fa-phone"></i>
						<span class="textmd">Phone :  {{ $footer->phone }}</span>
					</div>
					<div class="motdong">
						<i class="fa fa-envelope-o"></i>
						<span class="textmd" style="width: 100%">Email: {{ $footer->email }}</span>
                    </div>
                  
					

				</div>  <!-- HET COTF1 -->
				<div class="col-sm-1 push-sm-1 cotf2 mb-2  wow fadeInUp" data-wow-delay="0.1s">
					
				</div>  <!-- HET COTF2 -->
				<div class="col-sm-4  cotf2 mb-2 wow  fadeInUp" data-wow-delay="0.2s">
					<h2 class="tdft">THÔNG TIN THANH TOÁN</h2>
					<ul>
						<li><a href=""><i class="fa fa-hand-o-right"> Vietcombank: 0431000223398 </i> </a></li>
						<li><a href="">(Chi nhánh Phú Tài - Bình Định)</a></li>
						<li><a href=""><i class="fa fa-hand-o-right"> Agribank: 4303205136022  </i>  </a></li>
						<li><a href="">(Chi nhánh Phú Tài - Bình Định)</a></li>
						<li><a href=""><i class="fa fa-hand-o-right"> BIDV: 58110001082802 </i>  </a></li>
						<li><a href=""> (Chi nhánh Phú Tài - Bình Định) </a></li>
                    </ul>
                    <p style="font-size:18px;"><i class="fa fa-arrow-circle-right">Tên TK: Nguyễn Cao Ngọc Lợi</i> </p>
				</div>  <!-- HET COTF3 -->
				<div class="col-sm-3  cotf4 wow  fadeInUp" data-wow-delay="0.3s">
					<h2 class="tdft">GIỚI THIỆU </h2>
					 
					<p> {!! html_entity_decode( $footer->gioithieu) !!}</p>
				</div>  <!-- HET COTF4 -->
				@endforeach
			</div>
		</div>
	</div>  <!-- HET FOOTERTOP -->
   

    
<!--    <div class="bottom_support" style="z-index:999; width:100%">-->
<!--            <div class="wrap_bottom">-->
<!--                <div class="hotline_bottom">-->
<!--                    <span class="ico"><img src="http://tenten.vn/LandingPage/homepage/images/bottom_support_ico_phone.png"></span>-->
<!--                    <div class="txt"><a href="tel:0868999179">Hotline: <strong>0868.999.179</strong></a></div>-->
<!--                </div>-->
<!--                <div class="guide_payment">-->
<!--                    <a href="#">-->
<!--                        <span class="ico"><img src="http://tenten.vn/LandingPage/homepage/images/bottom_support_ico_guidepayment.png"></span>-->
<!--                        <span class="txt"></span></a><a href="tel:0868999179">Zalo: 0868999179</a>-->
                    
<!--                </div>-->
<!--                <div class="advisory_online">-->
<!--                    <a target="_blank" href="https://www.facebook.com/PotentialTeam">-->
<!--                        <span class="ico"><img src="http://tenten.vn/LandingPage/homepage/images/bottom_support_ico_advisoryonline.png"></span>-->
<!--                        <span class="txt">Nhắn tin Facebook</span>-->
<!--                    </a>-->
<!--                </div>-->
<!--            </div>-->
<!--    </div>-->
<!--        <style type="text/css" id="wp-custom-css">-->
<!--			.bottom_support{height:45px;width:100%;position:fixed;bottom:0;background:#32a22d;font-family:'Roboto Condensed', sans-serif;color:#fff;line-height:45px;z-index:9999}-->
<!--.bottom_support a{font-family:'Roboto Condensed', sans-serif;color:#fff;line-height:45px;font-size:18px}-->
<!--.wrap_bottom{max-width:1380px;margin:0 auto}-->
<!--.bottom_support .hotline_bottom{background:#32a22d;width:40%;float:left;font-size:16px;margin-left:5%;position:relative}-->
<!--.bottom_support .hotline_bottom .ico{width:16%;position:absolute;bottom:0}-->
<!--.bottom_support .hotline_bottom .txt{margin-left:16%}-->
<!--.bottom_support .hotline_bottom .ico img{max-width:100%}-->
<!--.bottom_support .hotline_bottom strong{font-weight:bold;font-size:19px}-->
<!--.bottom_support .hotline_bottom span{font-size:14px}-->
<!--.bottom_support .guide_payment{background:linear-gradient(-60deg, #177814 100%, #32a22d 50%) no-repeat;width:25%;float:left;position:relative}-->
<!--.bottom_support .guide_payment .ico{width:20%;position:absolute;bottom:-25%;left:15%}-->
<!--.bottom_support .guide_payment .ico img{max-width:100%}-->
<!--.bottom_support .guide_payment strong{font-weight:bold;font-size:19px}-->
<!--.bottom_support .guide_payment .txt{margin-left:35%}-->
<!--.bottom_support .advisory_online{background:linear-gradient(-60deg, #014f01 100%,  50%) no-repeat;width:30%;float:left;position:relative}-->
<!--.bottom_support .advisory_online .ico{width:20%;position:absolute;bottom:-10%;left:15%}-->
<!--.bottom_support .advisory_online .ico img{max-width:100%;float:left;margin-top:-20px}-->
<!--.bottom_support .advisory_online .txt{margin-left:35%}-->
<!--@media screen and (max-width:1200px){.bottom_support .hotline_bottom{width:45%}-->
<!--.bottom_support .hotline_bottom .ico{width:12%}-->
<!--.bottom_support .hotline_bottom .txt{margin-left:12%}-->
<!--.bottom_support .guide_payment{width:25%}-->
<!--.bottom_support .guide_payment .ico{bottom:-15%}-->
<!--.bottom_support .advisory_online{width:25%}-->
<!--.bottom_support .advisory_online .ico{bottom:0}-->
<!--}-->
<!--@media screen and (max-width:1000px){.bottom_support{height:40px;line-height:40px}-->
<!--.bottom_support a{font-size:13px;line-height:40px}-->
<!--.bottom_support .hotline_bottom{margin-left:2%;width:35%}-->
<!--.bottom_support .guide_payment .ico{bottom:0;left:7%}-->
<!--.bottom_support .guide_payment .txt{margin-left:27%}-->
<!--.bottom_support .advisory_online .ico{bottom:10%;left:10%}-->
<!--.bottom_support .advisory_online .txt{margin-left:30%}-->
<!--}-->
<!--@media screen and (max-width:820px){.bottom_support{height:30px;line-height:30px}-->
<!--.bottom_support a{font-size:15px;line-height:30px}-->
<!--.bottom_support .hotline_bottom{font-size:15px}-->
<!--.bottom_support .hotline_bottom strong{font-size:18px}-->
<!--.bottom_support .hotline_bottom span{font-size:12px}-->
<!--}-->
<!--@media screen and (max-width:720px){.bottom_support a,.bottom_support .hotline_bottom{font-size:13px}-->
<!--.bottom_support .hotline_bottom strong{font-size:15px}-->
<!--}-->
<!--@media screen and (max-width:620px){.bottom_support{height:25px;line-height:25px}-->
<!--.bottom_support a{font-size:11px;line-height:25px}-->
<!--.bottom_support .hotline_bottom{margin-left:0;width:50%;font-size:11px}-->
<!--.bottom_support .hotline_bottom strong{font-size:15px}-->
<!--.bottom_support .hotline_bottom span{font-size:11px}-->
<!--}-->
<!--@media screen and (max-width:560px){.bottom_support{height:20px;line-height:20px}-->
<!--.bottom_support img{display:none}-->
<!--.bottom_support a{font-size:10px;line-height:20px}-->
<!--.bottom_support .hotline_bottom,.bottom_support .hotline_bottom span{font-size:13px}-->
<!--.bottom_support .hotline_bottom strong{font-size:13px}-->
<!--.bottom_support .hotline_bottom .txt{margin-left:5%}-->
<!--.bottom_support .guide_payment .txt {margin-left:9%}-->
<!--.bottom_support .advisory_online .txt{margin-left:12%}-->
<!--}-->
<!--@media screen and (max-width:440px){.bottom_support .hotline_bottom{width:35%}-->
<!--.bottom_support{height:40px;width:100%;position:fixed;bottom:0;background:#32a22d;font-family:'Roboto Condensed', sans-serif;color:#fff;z-index:9999,}-->

<!--.bottom_support .guide_payment {width:32%}-->
<!--.bottom_support .advisory_online{width:33%}-->
<!--.bottom_support .guide_payment a span{font-size:13px}-->
<!--.bottom_support .advisory_online a span{font-size:12px}-->
<!--.bottom_support .hotline_bottom {padding-top: 10px;padding-bottom: 10px;}-->
<!--.bottom_support .guide_payment  {padding-top: 10px;padding-bottom: 10px}-->
<!--.bottom_support .advisory_online {padding-top: 10px;padding-bottom: 10px}-->
<!--}-->
<!--@media screen and (max-width:400px){.bottom_support a,.bottom_support .hotline_bottom,.bottom_support .hotline_bottom span{font-size:9px}-->
<!--.bottom_support .hotline_bottom strong{font-size:12px}-->
<!--}-->
<!--@media screen and (max-width:340px){.bottom_support a,.bottom_support .hotline_bottom,.bottom_support .hotline_bottom span{font-size:8px}-->
<!--}		</style>-->
<style>
        .icon{
            position: relative;
                            width: 45px;
                            height: 36px;
                            font-size: 45px;
                            color: #d82460;
                            text-align: center;
                            vertical-align: middle;
        }
        .vieni{
            position: relative;
                            display: block;
                            width: 100px;
                            height: 100px;
                            margin-left: auto;
                            margin-right: auto;
                            padding-top: 20px;
                            border: 8px solid #fff;
                            -webkit-border-radius: 100%;
                            -moz-border-radius: 100%;
                            -ms-border-radius: 100%;
                            -o-border-radius: 100%;
                            border-radius: 100%;
        }
        .khachhang{
            margin-top: 50px;
         
            background-size: cover;
            color: white;
           
            padding-bottom: 40px;
        }
        .khcan{
            height: 405px;
            width: 100%;
            background: #F6F7F9;
          
        }
        .title h2{
            position: relative;
            font-size: 36px;
            line-height: 50px;
            padding-top: 40px;
            display: block;
            
            margin-top: 0;
            font-weight: 400;
            font-family:'Roboto', sans-serif;
        }
        .fontdancing{
            font-family: 'utmavo';
            font-size: 36px;
        }
        .dichvu{
            padding-bottom: 20px;
            margin-top: -27px;
        }
        .dichvu1{
            padding-bottom: 20px;
             margin-top: -7px;
        
        }
         .dichvu2{
            padding-bottom: 20px;
             margin-top: 23px;
        
        }
         .dichvu3{
            padding-bottom: 20px;
             margin-top: 23px;
        
        }
        .la{
            color:#dddddd;
            font-size:12px;
            padding:0 13px;
        }
        .datban1 {
    
            float: left;
            margin-left: -14px;
            font-family: 'utravo';
            font-size: 13px;
            color: #ddd;
        }
        .datban1.t2{
            
             padding-left: 40px;
        }
        .len{
            font:normal normal normal 15px / 19px "Roboto", Helvetica, Arial, Verdana, sans-serif;
            
        }
        .nav.navbar-nav.len1 {
            margin-top: -11px;
        }
       
      
        .row1{
            border-bottom:1px solid;
        }
        .mangxh{
            margin-right: -28px;
        }
        .qt{
            margin-bottom:-55px;
        }
        .navbar-toggler{
           background:#eceeef no-repeat center center;
        }
       
       
        .mySlides {display:none;}
         </style>
        

        <!--  <script type="text/javascript">-->
        
        <!--    document.write('<style>body{padding-bottom:20px}#e_itexpress_left{display:none;position:fixed;z-index:9999;top:0;left:0}#e_itexpress_right{display:none;position:fixed;z-index:9999;top:0;right:0}#e_itexpress_footer{display:none;position:fixed;z-index:9999;bottom:-50px;left:0;width:100%;height:104px; repeat-x bottom left}#e_itexpress_bottom_left{display:none;position:fixed;z-index:9999;bottom:20px;left:20px}@media (min-width: 992px){#e_itexpress_left,#e_itexpress_right,#e_itexpress_footer,#e_itexpress_bottom_left{display:block}}</style><img id="e_itexpress_left" src="http://demo.iwebs.vn/api/images/noel/topleft.png"/><img id="e_itexpress_right" src="http://demo.iwebs.vn/api/images/noel/topright.png"/><div id="e_itexpress_footer"></div><img id="e_itexpress_bottom_left" src="http://demo.iwebs.vn/api/images/noel/bottomleft.png"/><div style="position:fixed;z-index:9999;bottom:3px;right:3px; font-size:12px;color:#8D8D8D;"></div>');-->
        <!--    var no=100;var hidesnowtime=0;var snowdistance='pageheight';var ie4up=(document.all)?1:0;var ns6up=(document.getElementById&&!document.all)?1:0;function iecompattest(){return(document.compatMode&&document.compatMode!='BackCompat')?document.documentElement:document.body}var dx,xp,yp;var am,stx,sty;var i,doc_width=800,doc_height=600;if(ns6up){doc_width=self.innerWidth;doc_height=self.innerHeight}else if(ie4up){doc_width=iecompattest().clientWidth;doc_height=iecompattest().clientHeight}dx=new Array();xp=new Array();yp=new Array();am=new Array();stx=new Array();sty=new Array();for(i=0;i<no;++i){dx[i]=0;xp[i]=Math.random()*(doc_width-50);yp[i]=Math.random()*doc_height;am[i]=Math.random()*20;stx[i]=0.02+Math.random()/10; sty[i]=0.7+Math.random();if(ie4up||ns6up){document.write('<div id="dot'+i+'" style="POSITION:absolute;Z-INDEX:'+i+';VISIBILITY:visible;TOP:15px;LEFT:15px;"><span style="font-size:18px;color:#fff">*</span></div>')}}function snowIE_NS6(){doc_width=ns6up?window.innerWidth-10:iecompattest().clientWidth-10;doc_height=(window.innerHeight&&snowdistance=='windowheight')?window.innerHeight:(ie4up&&snowdistance=='windowheight')?iecompattest().clientHeight:(ie4up&&!window.opera&&snowdistance=='pageheight')?iecompattest().scrollHeight:iecompattest().offsetHeight;for(i=0;i<no;++i){yp[i]+=sty[i];if(yp[i]>doc_height-50){xp[i]=Math.random()*(doc_width-am[i]-30);yp[i]=0;stx[i]=0.02+Math.random()/10;sty[i]=0.7+Math.random()}dx[i]+=stx[i];document.getElementById('dot'+i).style.top=yp[i]+'px';document.getElementById('dot'+i).style.left=xp[i]+am[i]*Math.sin(dx[i])+'px'}snowtimer=setTimeout('snowIE_NS6()',10)}function hidesnow(){if(window.snowtimer){clearTimeout(snowtimer)}for(i=0;i<no;i++)document.getElementById('dot'+i).style.visibility='hidden'}if(ie4up||ns6up){snowIE_NS6();if(hidesnowtime>0)setTimeout('hidesnow()',hidesnowtime*1000)}-->
        <!--    var r=document.createElement("script");r.type="text/javascript";r.async=true;r.src=n+"//itexpress.vn/js/popup_newtab_time.js";-->
        <!--</script> -->

        


 
</body>
</html>