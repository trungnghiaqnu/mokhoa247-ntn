@extends('backend.master')
@section('main')
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"> ĐÁNH GIÁ</h1>
                </div>
            </div><!--/.row-->
            
            
            <div class="row">
                <div class="col-xs-12 ">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Sửa đánh giá
                            </div>
                            @include('error.note')
                            <div class="panel-body">
                                <form method="POST" action=" " enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>Ảnh</label>
                                        <input id="img" type="file" name="img" class="form-control " onchange="changeImg(this)">
                                        <img id="avatar" class="thumbnail"  src="{{asset('storage/app/img/'.$danhgia->img)}}" style="width:200px;height:200px">
                                    </div>
                                    <div class="form-group">
                                            <label>Tên</label>
                                            <input  required type="text" name="name" class="form-control" value="{{ $danhgia->name }}">
                                    </div>
                                    <div class="form-group">
                                            <label>Nội Dung</label>
                                            <textarea  required  name="noidung" id="noidung"  cols="30" rows="10" >
                                                    {{ $danhgia->noidung }}
                                            </textarea>
                                            <script>
                                                CKEDITOR.replace( 'noidung', {
                                                filebrowserBrowseUrl: 'ckfinder/ckfinder.html',
                                                filebrowserImageBrowseUrl:  'ckfinder/ckfinder.html?Type=Images',
                
                                                filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?type=Flash',
                                                filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                                                filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                                                filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
                                                });
                                            </script>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                            <input type="submit" name="submit" value="Lưu" class="form-control btn btn-primary"><a href="{{ asset('admin/danhgia') }}" class="form-control btn btn-danger" value="">Hủy</a>
                                    </div>
                                    {{ csrf_field()}}
                                </form>
                            </div>
                        </div>
                </div>
                
            </div>
            
                
    </div>	<!--/.main-->

@stop