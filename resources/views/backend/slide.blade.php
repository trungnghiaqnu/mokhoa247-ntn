@extends('backend.master')
@section('main')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Slide</h1>
			</div>
		</div><!--/.row-->
		
		
		<div class="row">
			<div class="col-xs-12 col-md-5 col-lg-5">
					<div class="panel panel-primary">
						<div class="panel-heading">
							Thêm slide
						</div>
						@include('error.note')
						<div class="panel-body">
							<form method="POST" action="{{  asset('admin/slide/add')}} " enctype="multipart/form-data">
								<div class="form-group">
									<label>Ảnh Slide</label>
                                    <input required id="img" type="file" name="img" class="form-control " onchange="changeImg(this)" >
								</div>
								<div class="form-group">
									<input class="form-control btn btn-primary" type="submit" value="Thêm">
								</div>
								{{ csrf_field()}}
                            </form>
                        </div>
					</div>
			</div>
			<div class="col-xs-12 col-md-7 col-lg-7">
					<div class="panel panel-primary">
						<div class="panel-heading">Danh sách slide</div>
						<div class="col-lg-12">
							@if (Session::has('flash_message'))
								<div class="alert alert-success">
									{{ Session::get('flash_message') }}
								</div>
								
							@endif
						</div>
						<div class="panel-body">
							<div class="bootstrap-table">
								<table class="table table-bordered">
									  <thead>
										<tr class="bg-primary">
											<th>ID</th>
										  <th>Ảnh slide</th>
										  <th style="width:30%">Tùy chọn</th>
										</tr>
									  </thead>
									  <tbody>
										@foreach($slide as $slide)
											<tr>
												<td>
														{{ $slide->id }}
												</td>
												<td>
													<img height="130px" src="{{ asset('storage/app/img/'.$slide->img) }}" class="thumbnail">
												</td>
												<td>
													
													<a href="{{ asset('admin/slide/delete/'.$slide->id) }}" onclick="return confirm('Bạn có chắc chắn muốn xóa?')" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Xóa</a>
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
		</div>
		
			
</div>	<!--/.main-->

@stop