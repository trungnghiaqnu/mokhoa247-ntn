@extends('backend.master')
@section('main')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Footer</h1>
			</div>
		</div><!--/.row-->
		
		
		<div class="row">
			<div class="col-xs-12 ">
					<div class="panel panel-primary">
						<div class="panel-heading">
						Nội Dung
						</div>
						@include('error.note')
						<div class="panel-body">
							<form method="POST" action="" enctype="multipart/form-data">
								<div class="form-group">
									<label>Sđt</label>
                                    <input  required type="text" name="phone" class="form-control" value="{{ $footer->phone }}">
                                </div>
                            
                                <div class="form-group">
                                        <label>Mail</label>
                                        <input  required type="text" name="mail" class="form-control" value="{{ $footer->email }}">
								</div>
								<div class="form-group">
									<label>Địa chỉ</label>
									<input  required type="text" name="diachi" class="form-control" value="{{ $footer->diachi }}">
								</div>
                                <div class="form-group">
									<label>Giới thiệu</label>
									<textarea  required  name="gioithieu" id="noidung"  >
										{{ $footer->gioithieu }}
									</textarea>
									<script>
										CKEDITOR.replace( 'noidung', {
										filebrowserBrowseUrl: 'ckfinder/ckfinder.html',
										filebrowserImageBrowseUrl:  'ckfinder/ckfinder.html?Type=Images',
		
										filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?type=Flash',
										filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
										filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
										filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
										});
									</script>
							</div>
                                <hr>
								<div class="form-group">
									<input class="form-control btn btn-primary" type="submit" value="Lưu"> <a href="{{ asset('admin/footer') }}" class="form-control btn btn-danger" value="">Hủy</a>
								</div>
								{{ csrf_field()}}
                            </form>
                        </div>
					</div>
			</div>
			
		</div>
	</div>
@stop