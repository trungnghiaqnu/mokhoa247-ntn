@extends('backend.master')
@section('main')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Footer</h1>
			</div>
		</div><!--/.row-->
		
		
		<div class="row">
			<div class="col-xs-12 ">
					<div class="panel panel-primary">
						<div class="panel-heading">
						Nội Dung
						</div>
						@include('error.note')
						<div class="panel-body">
							<form method="POST" action="{{  asset('admin/footer/add')}} " enctype="multipart/form-data">
								<div class="form-group">
									<label>Sđt</label>
                                    <input  required type="text" name="phone" class="form-control" id="">
                                </div>
                            
                                <div class="form-group">
                                        <label>Mail</label>
                                        <input  required type="text" name="mail" class="form-control" id="">
								</div>
								<div class="form-group">
									<label>Địa chỉ</label>
									<input  required type="text" name="diachi" class="form-control" id="">
								</div>
                                <div class="form-group">
                                        <label>Giới thiệu</label>
										<textarea  required  name="gioithieu" id="noidung"  ></textarea>
										<script>
											CKEDITOR.replace( 'noidung', {
											filebrowserBrowseUrl: 'ckfinder/ckfinder.html',
											filebrowserImageBrowseUrl:  'ckfinder/ckfinder.html?Type=Images',
			
											filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?type=Flash',
											filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
											filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
											filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
											});
										</script>
                                </div>
                                <hr>
								<div class="form-group">
									<input class="form-control btn btn-primary" type="submit" value="Thêm">
								</div>
								{{ csrf_field()}}
                            </form>
                        </div>
					</div>
			</div>
			
		</div>
		<div class="clearfix"></div>
		<div class="row">
		<div class="col-xs-12 ">
			<div class="panel panel-primary">
				<div class="panel-heading">Danh sách</div>
				<div class="col-lg-12">
					@if (Session::has('flash_message'))
						<div class="alert alert-success">
							{{ Session::get('flash_message') }}
						</div>
						
					@endif
				</div>
				<div class="panel-body">
					<div class="bootstrap-table">
						<table class="table table-bordered">
							  <thead>
								<tr class="bg-primary">
								  <th>ID</th>
								  <th>Sđt </th>
								  <th>Mail </th>
								  <th>Giới thiệu </th>
								  <th>Tùy chọn</th>
								</tr>
							  </thead>
							  <tbody>
								@foreach($footer as $footer)
									<tr>
										<td>
												{{ $footer->id }}
										</td>
										<td>
											<h4 class="card-title">{{ $footer->phone }}</h4>
										</td>
										<td>
												<h4 class="card-title">{{ $footer->email }}</h4>
											  
										</td>
										<td>
												<p class="card-text">{!! html_entity_decode($footer->gioithieu) !!}</p>
										</td>
										<td>
											<a href=" {{ asset('admin/footer/edit/'.$footer->id) }} " class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Sửa</a>
											<a href=" {{ asset('admin/footer/delete/'.$footer->id) }} " onclick="return confirm('Bạn có chắc chắn muốn xóa?')" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Xóa</a>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					
				</div>
			</div>
		</div>
	</div>
		
			
</div>	<!--/.main-->

@stop