@extends('backend.master')
@section('main')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">ĐÁNH GIÁ</h1>
			</div>
		</div><!--/.row-->
		
		
		<div class="row">
			<div class="col-xs-12 col-md-5 col-lg-5">
					<div class="panel panel-primary">
						<div class="panel-heading">
							Thêm đánh giá
						</div>
						@include('error.note')
						<div class="panel-body">
							<form method="POST" action="{{  asset('admin/danhgia/add')}} " enctype="multipart/form-data">
								<div class="form-group">
									<label>Ảnh</label>
                                    <input required id="img" type="file" name="img" class="form-control " onchange="changeImg(this)" >
                                </div>
                                <div class="form-group">
                                        <label>Tên</label>
                                        <input  required type="text" name="name" class="form-control" id="">
                                </div>
                                <div class="form-group">
                                        <label>Nội Dung</label>
										<textarea  required  name="noidung" id="noidung"  cols="30" rows="10" ></textarea>
										<script>
											CKEDITOR.replace( 'noidung', {
											filebrowserBrowseUrl: 'ckfinder/ckfinder.html',
											filebrowserImageBrowseUrl:  'ckfinder/ckfinder.html?Type=Images',
			
											filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?type=Flash',
											filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
											filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
											filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
											});
										</script>
                                </div>
                                <hr>
								<div class="form-group">
									<input class="form-control btn btn-primary" type="submit" value="Thêm">
								</div>
								{{ csrf_field()}}
                            </form>
                        </div>
					</div>
			</div>
			<div class="col-xs-12 col-md-7 col-lg-7">
					<div class="panel panel-primary">
						<div class="panel-heading">Danh sách đánh giá</div>
						<div class="col-lg-12">
							@if (Session::has('flash_message'))
								<div class="alert alert-success">
									{{ Session::get('flash_message') }}
								</div>
								
							@endif
						</div>
						<div class="panel-body">
							<div class="bootstrap-table">
								<table class="table table-bordered">
									  <thead>
										<tr class="bg-primary">
										  <th>ID</th>
                                          <th>Ảnh </th>
                                          <th>Tên </th>
                                          <th>Nội Dung </th>
										  <th style="width:30%">Tùy chọn</th>
										</tr>
									  </thead>
									  <tbody>
										@foreach($danhgia as $danhgia)
											<tr>
												<td>
														{{ $danhgia->id }}
												</td>
												<td>
													<img height="100px" src="{{ asset('storage/app/img/'.$danhgia->img) }}" class="thumbnail">
                                                </td>
                                                <td>
                                                        <h4 class="card-title">{{ $danhgia->name }}</h4>
                                                      
                                                </td>
                                                <td>
                                                        <p class="card-text">{!! html_entity_decode($danhgia->noidung ) !!}</p>
                                                </td>
												<td>
													<a href=" {{ asset('admin/danhgia/edit/'.$danhgia->id) }} " class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Sửa</a>
													<a href=" {{ asset('admin/danhgia/delete/'.$danhgia->id) }} " onclick="return confirm('Bạn có chắc chắn muốn xóa?')" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Xóa</a>
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
		</div>
		
			
</div>	<!--/.main-->

@stop