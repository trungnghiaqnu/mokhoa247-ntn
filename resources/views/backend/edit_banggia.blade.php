@extends('backend.master')
@section('main')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Danh mục bảng giá</h1>
			</div>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-xs-12 ">
					<div class="panel panel-primary">
						<div class="panel-heading">
							Sửa bảng giá
						</div>
						<div class="panel-body">
								@include('error.note')
							<form method="post" enctype="multipart/form-data">
                                    <div class="form-group" >
                                            <label>Ảnh bảng giá</label>
                                            <input id="img" type="file" name="img" class="form-control " onchange="changeImg(this)">
                                            <img id="avatar" class="thumbnail" width="400px" src="{{asset('storage/app/img/'.$banggia->img)}}">
                                        </div>
								<div class="form-group">
										<input type="submit" name="submit" class="form-control btn btn-primary" value="Lưu"><a href="{{ asset('admin/banggia') }}" class="form-control btn btn-danger" value="">Hủy</a>
								</div>
								
								{{ csrf_field()}}
							</form>
						</div>
					</div>
			</div>
		</div><!--/.row-->
	</div>	<!--/.main-->
@stop