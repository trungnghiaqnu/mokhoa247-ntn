<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','FrontendController@getHome')->name('home');


Route::get('/{slug?}.html','FrontendController@getDetailBaiViet')->where('slug','[ \/\w\.-]*')->name('postDetail');





Route::group(['namespace'=>'Admin'],function(){ 
    Route::group(['prefix'=>'login','middleware'=>'CheckLogin'],function(){
        Route::get('/','LoginController@getLogin');
        Route::post('/','LoginController@postLogin');
    });
    Route::get('logout','HomeController@getLogout');

    Route::group(['prefix'=>'admin','middleware'=>'CheckLogOut'],function(){
        Route::get('home','HomeController@getHome');

        // cate
        Route::group(['prefix'=>'cate'],function(){
            Route::get('/','CateController@getCate');
            Route::post('add','CateController@postAddCate');
            Route::get('edit/{cate_id}','CateController@getEditCate');
            Route::post('edit/{cate_id}','CateController@postEditCate');
            Route::get('delete/{cate_id}','CateController@getDeleteCate');
        });

        // slide
        Route::group(['prefix'=>'slide'],function(){
            Route::get('/','SlideController@getSlide');
            Route::post('add','SlideController@postAdd');
            Route::get('delete/{id}','SlideController@getDeleteSlide');
        });

        //bảng giá
        Route::group(['prefix'=>'banggia'],function(){
            Route::get('/','BanggiaController@getBangGia');
            Route::post('add','BanggiaController@postAddBangGia'); 
            Route::get('edit/{id}','BanggiaController@getEditBangGia');
            Route::post('edit/{id}','BanggiaController@postEditBangGia');
            
            Route::get('delete/{id}','BanggiaController@getDeleteBangGia');
        });
        
        //bài viết
        Route::group(['prefix'=>'baiviet'],function(){
            Route::get('/','BaivietController@getBaiViet');

            Route::post('add','BaivietController@postAddBaiViet');

            Route::get('edit/{id}','BaivietController@getEditBaiViet');
            Route::post('edit/{id}','BaivietController@postEditBaiViet');

            Route::get('delete/{id}','BaivietController@getDeleteBaiViet');
            Route::get('detail/{id}','BaivietController@getview');
           
        });

        // đánh giá
        Route::group(['prefix'=>'danhgia'],function(){
            Route::get('/','DanhgiaController@getDanhGia');

            Route::post('add','DanhgiaController@postAddDanhGia');

            Route::get('edit/{id}','DanhgiaController@getEditDanhGia');
            Route::post('edit/{id}','DanhgiaController@postEditDanhGia');

            Route::get('delete/{id}','DanhgiaController@getDeleteDanhGia');
           
        });

        //footer
        Route::group(['prefix'=>'footer'],function(){
            Route::get('/','FooterController@getFooter');

            Route::post('add','FooterController@postAddFooter');

            Route::get('edit/{id}','FooterController@getEditFooter');
            Route::post('edit/{id}','FooterController@postEditFooter');

            Route::get('delete/{id}','FooterController@getDeleteFooter');
           
        });

  
    });
});
