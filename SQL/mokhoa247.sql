-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th12 13, 2018 lúc 04:52 PM
-- Phiên bản máy phục vụ: 10.1.26-MariaDB
-- Phiên bản PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `mokhoa247`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `baiviets`
--

CREATE TABLE `baiviets` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `trichdan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `noidung` text COLLATE utf8_unicode_ci NOT NULL,
  `baiviet_cate` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `baiviets`
--

INSERT INTO `baiviets` (`id`, `title`, `slug`, `trichdan`, `img`, `noidung`, `baiviet_cate`, `created_at`, `updated_at`) VALUES
(8, 'Dịch vụ tăng share trên facebook', 'dich-vu-tang-share-tren-facebook', 'Nhiều người hâm mộ khẳng định Real đã không tôn trọng đối thủ ở trận chung kết khi mặc áo số 13 tượng trưng cho 13 lần vô địch Champions League ngay từ trận bán kết.', '442424.PNG', '<p>Tr&ugrave;m cờ bạc Nguyễn Văn Dương khai đ&atilde; cho &ocirc;ng Vĩnh 27 tỷ c&ugrave;ng hơn 1,7 triệu USD v&agrave; tặng &ocirc;ng H&oacute;a 22 tỷ đồng. Tuy nhi&ecirc;n, hai cựu c&aacute;n bộ c&ocirc;ng an phủ nhận.</p>\r\n\r\n<p><strong><a href=\"https://news.zing.vn/video-ong-phan-van-vinh-nguyen-thanh-hoa-bao-ke-duong-day-danh-bac-the-nao-post884572.html\" target=\"_blank\">&Ocirc;ng Phan Văn Vĩnh, Nguyễn Thanh H&oacute;a bảo k&ecirc; đường d&acirc;y đ&aacute;nh bạc thế n&agrave;o?</a></strong>&nbsp;Báo cáo lãnh đạo kh&ocirc;ng trung thực v&ecirc;̀ Rikvip, &ocirc;ng Vĩnh và &ocirc;ng Hóa tìm cách bao che sai phạm và đ&ecirc;̀ nghị B&ocirc;̣ TT&amp;TT c&acirc;́p phép cho game đánh bạc &quot;chui&quot; này.</p>\r\n\r\n<p>S&aacute;ng 13/11, VKSND tỉnh Ph&uacute; Thọ tiếp tục tr&igrave;nh b&agrave;y bản c&aacute;o trạng d&agrave;i 235 truy tố &ocirc;ng&nbsp;<a href=\"https://news.zing.vn/tieu-diem/phan-van-vinh.html\">Phan Văn Vĩnh</a>, nguy&ecirc;n Tổng cục trưởng Tổng cục cảnh s&aacute;t v&agrave; 91 bị c&aacute;o trong đường d&acirc;y đ&aacute;nh bạc ngh&igrave;n tỷ.</p>\r\n\r\n<p>Sau khi đề cập c&aacute;c h&agrave;nh vi đ&aacute;nh bạc, tổ chức đ&aacute;nh bạc, rửa tiền&hellip; kiểm s&aacute;t vi&ecirc;n đ&atilde; tr&igrave;nh b&agrave;y nội dung li&ecirc;n quan đến việc tr&ugrave;m cờ bạc về khai hối lộ &ocirc;ng Vĩnh v&agrave; cấp dưới h&agrave;ng chục tỷ đồng</p>\r\n\r\n<h3>43 triệu t&agrave;i khoản đ&aacute;nh bạc</h3>\r\n\r\n<p>Theo kết quả trung cầu gi&aacute;m định, từ th&aacute;ng 4/2015 đến 8/2017, hệ thống đ&aacute;nh bạc c&oacute; gần 43 triệu t&agrave;i khoản người chơi, hơn 5.900 t&agrave;i khoản đại l&yacute;. Viện Khoa học H&igrave;nh sự Bộ C&ocirc;ng an x&aacute;c định c&oacute; hơn 500 t&agrave;i khoản đặt cược trong một lần từ 5 triệu đồng trở l&ecirc;n.</p>\r\n\r\n<table align=\"center\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<ol>\r\n				<li><img alt=\"Nguyen Van Duong khai tang Rolex, trieu USD cho ong Phan Van Vinh hinh anh 1\" src=\"https://znews-photo.zadn.vn/w1024/Uploaded/jopltui/2018_11_08/Nguyen_Thanh_Hoa_zing.jpg\" /></li>\r\n			</ol>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Bị c&aacute;o Nguyễn Thanh H&oacute;a, cựu Cục trưởng C50. Ảnh:&nbsp;<em>Việt Linh.</em></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Căn cứ bi&ecirc;n bản đối so&aacute;t, t&agrave;i liệu lưu do Phan S&agrave;o Nam cung cấp v&agrave; thu thập được, cơ quan chức năng x&aacute;c định c&oacute; hơn 9.850 tỷ được nạp v&agrave;o hệ thống game đ&aacute;nh bạc do Nguyễn Văn Dương v&agrave; đồng phạm vận h&agrave;nh. Kiểm s&aacute;t vi&ecirc;n cho rằng số liệu n&agrave;y chưa đầy đủ v&igrave; kh&ocirc;ng c&oacute; dữ liệu&nbsp;<img alt=\"\" src=\"/mokhoa247/public/images/images/cau-thang-kinh.jpg\" style=\"height:345px; width:480px\" /></p>', 1, '2018-11-13 09:13:25', '2018-11-13 09:13:25');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `banggias`
--

CREATE TABLE `banggias` (
  `id` int(10) UNSIGNED NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `banggias`
--

INSERT INTO `banggias` (`id`, `img`, `created_at`, `updated_at`) VALUES
(3, 'mokhoafacebook.png', '2018-12-09 15:57:40', '2018-12-09 15:57:40');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `cates`
--

CREATE TABLE `cates` (
  `cate_id` int(10) UNSIGNED NOT NULL,
  `cate_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cate_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `cates`
--

INSERT INTO `cates` (`cate_id`, `cate_name`, `cate_slug`, `created_at`, `updated_at`) VALUES
(1, 'MỞ KHÓA FACEBOOK', 'mo-khoa-facebook', '2018-11-10 08:21:54', '2018-11-10 08:21:54'),
(2, 'DỊCH VỤ FACEBOOK', 'dich-vu-facebook', '2018-11-10 08:22:14', '2018-11-10 08:22:14'),
(3, 'TĂNG LIKE', 'tang-like', '2018-11-10 08:22:24', '2018-11-10 08:40:31'),
(4, 'LIÊN HỆ', 'lien-he', '2018-11-10 08:22:45', '2018-11-10 08:22:45');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `danhgias`
--

CREATE TABLE `danhgias` (
  `id` int(10) UNSIGNED NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noidung` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `danhgias`
--

INSERT INTO `danhgias` (`id`, `img`, `name`, `noidung`, `created_at`, `updated_at`) VALUES
(1, 'thuylao.jpg', 'Thúy Láo', 'Láo đã sử dụng dịch vụ của DICHVUFB.NET và Láo cảm thấy rất hài lòng về đội ngũ làm việc. Họ thực hiện yêu cầu của khách hàng nhanh chóng, chất lượng, hỗ trợ rất nhiệt tình, còn bảo hành nữa.', '2018-11-13 03:28:37', '2018-11-13 03:43:09'),
(2, 'danhgia3.jpg', 'NyNy', 'Mình bán hàng online, lang thang trên mạng và tìm được trang DICHVUFB.NET tuyệt vời này. Từ khi tham gia dịch vụ, lượng hàng mình bán ngày càng tăng đồng nghĩa với việc doanh thu cũng tăng lên rất nhiều.', '2018-11-13 03:45:33', '2018-11-13 03:45:33');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `footers`
--

CREATE TABLE `footers` (
  `id` int(10) UNSIGNED NOT NULL,
  `gioithieu` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diachi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `footers`
--

INSERT INTO `footers` (`id`, `gioithieu`, `phone`, `email`, `diachi`, `created_at`, `updated_at`) VALUES
(1, 'chuyên cung cấp các dịch vụ mạng xã hội như: Facebook, Youtube, Google, Instagram, ... Chúng tôi là giải pháp cho khách hàng kinh doanh mặt hàng online hướng tới người dùng tiềm năng, giúp tăng doanh số bán hàng và đẩy mạnh lượng người tương tác hiệu quả nhất.', '0868.999.179', 'dichvufb.hotro@gmail.com', '67 Mai Chí Thọ, Quận 2, TP. HCM', '2018-12-10 16:28:03', '2018-12-10 16:28:03');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_11_08_081502_user', 1),
(3, '2018_11_08_082917_user', 2),
(4, '2018_11_08_154102_user', 3),
(5, '2018_11_08_162340_slide', 4),
(6, '2018_11_09_091239_banggia', 5),
(7, '2018_11_10_145744_cate', 6),
(8, '2018_11_10_162503_baiviet', 7),
(9, '2018_11_10_173849_baiviet', 8),
(10, '2018_11_13_100536_danh_gia', 9),
(11, '2018_12_09_220505_footer', 10);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `sliders`
--

INSERT INTO `sliders` (`id`, `img`, `created_at`, `updated_at`) VALUES
(1, 'dichvufb.png', '2018-11-08 15:50:01', '2018-11-08 15:50:01'),
(2, 'dichvufb2.png', '2018-11-08 15:50:40', '2018-11-08 15:50:40');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` tinyint(4) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$aHyVJhmhEXtV/wry5hrS0OEmG6wEj0uI5KCReNQ/lUNksHGCUs7kK', 1, 'eP6z0lbbfQ7fT8qj9Ly0w4bf5oz2I8TEKga4BxXcnTzLNmzCdVXogw3UUmO1', NULL, NULL);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `baiviets`
--
ALTER TABLE `baiviets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `baiviets_baiviet_cate_foreign` (`baiviet_cate`);

--
-- Chỉ mục cho bảng `banggias`
--
ALTER TABLE `banggias`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `cates`
--
ALTER TABLE `cates`
  ADD PRIMARY KEY (`cate_id`);

--
-- Chỉ mục cho bảng `danhgias`
--
ALTER TABLE `danhgias`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `footers`
--
ALTER TABLE `footers`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `baiviets`
--
ALTER TABLE `baiviets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `banggias`
--
ALTER TABLE `banggias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `cates`
--
ALTER TABLE `cates`
  MODIFY `cate_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `danhgias`
--
ALTER TABLE `danhgias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `footers`
--
ALTER TABLE `footers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `baiviets`
--
ALTER TABLE `baiviets`
  ADD CONSTRAINT `baiviets_baiviet_cate_foreign` FOREIGN KEY (`baiviet_cate`) REFERENCES `cates` (`cate_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
