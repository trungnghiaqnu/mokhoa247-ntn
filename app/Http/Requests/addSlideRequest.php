<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class addSlideRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'img'=>'image',
            'img'=>'unique:sliders,img'
        ];
    }
    public function messages(){
        return [
            'img.unique'=>'Ảnh đã bị trùng, xin vui lòng nhập ảnh khác !'
        ];
    }
}
