<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\banggia;
use App\Http\Requests\addBanggiaRequest;

class BanggiaController extends Controller
{
    public function getBangGia(){
        $data['banggia'] = banggia::all();
        return view('backend.banggia',$data);
    }
    public function postAddBangGia(addBanggiaRequest $request){
        $filename = $request->img->getClientOriginalName();
        $slide = new banggia;
        $slide->img = $filename;
        $slide->save();
        $request->img->storeAs('img',$filename);
        return redirect('admin/banggia')->with(['flash_message'=>'Bạn thêm thành công']);
      

     }
     public function getEditBangGia($id){
        $data['banggia'] = banggia::find($id);
       
        return view('backend.edit_banggia',$data);
    }
    public function postEdit(addBanggiaRequest $request, $id){
        $banggia = new banggia;

        if($request->hasFile('img')){
            $img = $request->img->getclientOriginalName();// lấy về cái tên file ảnh
            $arr['img'] = $img;//truyền vào mảng
            $request->img->storeAs('img',$img); // lưu vào trong file có tên img
        }

        $banggia::where('id',$id)->update($arr);
        return redirect('admin/banggia')->with('flash_message','Bạn sửa thành công !!');

    }
    public function getDeleteBangGia($id){
        banggia::destroy($id);
        return back()->with('flash_message','Bạn xóa thành công !!');
    }

}

