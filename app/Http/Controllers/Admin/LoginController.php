<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class LoginController extends Controller
{
    public function __construct()
    {
       
    }

    public function getLogin(){
        if (Auth::check()) {
            // nếu đăng nhập thàng công thì 
            return redirect('admin/home');
        } else {
            return view('backend/login');
        }
        //return view('backend.login');
    } 

    public function postLogin(Request $request){
        $array = ['username' => $request->username, 
                  'password' => $request->password,
                  'level' => 1,
                 ];
        
        if(Auth::attempt($array)){
            return redirect('admin/home');
        }
        else{
            return back()->with('error','Please again !!');
        }

    }
}
