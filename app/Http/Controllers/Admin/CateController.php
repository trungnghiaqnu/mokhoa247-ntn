<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\cate;
use App\Http\Requests\addCateRequest;
use App\Http\Requests\editCateRequest;

class CateController extends Controller
{
    public function getCate(){
        $data['catelist'] = cate::all();
        return view('backend.cate',$data);
     }
     public function postAddCate(addCateRequest $request){
        $cate = new cate;
        $cate->cate_name = $request->name;
        $cate->cate_slug = str_slug($request->name);
        $cate-> save();
        return redirect('admin/cate')->with(['flash_message'=>'Bạn thêm thành công']);

    }
    public function getEditCate($cate_id){

        $data['cate'] = cate::find($cate_id);
        return view('backend.edit_cate',$data);
    }
    public function postEditCate(editCateRequest $request, $cate_id){

        $cate = cate::find($cate_id);
        $cate->cate_name = $request->name;
        $cate->cate_slug = str_slug($request->name);
        $cate-> save();
        return redirect('admin/cate')->with(['flash_message'=>'Bạn sửa thành công !!']);
    }
    public function getDeleteCate($cate_id){
        cate::destroy($cate_id);
        return back()->with(['flash_message'=>'Bạn xóa thành công !!']);
    }
}
