<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\slide;
use App\Http\Requests\addSlideRequest;

class SlideController extends Controller
{
    public function getSlide(){
        $data['slide'] = slide::all();
        return view('backend.slide',$data);
    }
    public function postAdd(addSlideRequest $request){
        $filename = $request->img->getClientOriginalName();
        $slide = new slide;
        $slide->img = $filename;
        $slide->save();
        $request->img->storeAs('img',$filename);
        return redirect('admin/slide')->with(['flash_message'=>'Bạn thêm thành công']);
     }
     
    public function getDeleteSlide($id){
        slide::destroy($id);
        return back()->with('flash_message','Bạn xóa thành công !!');
    }

}
