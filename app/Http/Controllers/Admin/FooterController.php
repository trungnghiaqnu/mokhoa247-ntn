<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\footer;

class FooterController extends Controller
{
    public function getFooter(){

        $data['footer'] = footer::all();
        return view('backend.footer',$data);
    }
    public function postAddFooter(Request $request){
       
        $footer = new footer;
        $footer->phone =$request->phone;
        $footer->email= $request->mail;
        $footer->diachi= $request->diachi;
        $footer->gioithieu= $request->gioithieu;
        $footer->save();
       
        return redirect('admin/footer')->with(['flash_message'=>'Bạn thêm thành công']);
      

    }
    public function getEditFooter($id){
        $data['footer'] = footer::find($id);
       
        return view('backend.edit_footer',$data);

        
    }
    public function postEditFooter(Request $request,$id){
        $footer = new footer;
        $arr['phone'] = $request->phone;
        $arr['email'] = $request->mail;
        $arr['diachi'] = $request->diachi;
        $arr['gioithieu'] = $request->gioithieu;
       
        $footer::where('id',$id)->update($arr);
        return redirect('admin/footer')->with('flash_message','Bạn sửa thành công !!');
        
    }
  
    public function getDeleteFooter($id){
        footer::destroy($id);
        return back()->with('flash_message','Bạn xóa thành công !!');
        
    }
}
