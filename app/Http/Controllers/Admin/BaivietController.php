<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\baiviet;
use App\Model\cate;
use App\Http\Requests\addBaivietRequest;
use DB;

class BaivietController extends Controller
{
    public function getBaiViet(){ 
        $data['baivietlist'] = DB::table('baiviets')->join('cates','baiviets.baiviet_cate','=','cates.cate_id')->orderBy('id','desc')->get();
        $data['catelist'] = cate::all();
        return view('backend.baiviet',$data);
    }
    public function postAddBaiViet(addBaivietRequest $request){ 
        $filename = $request->img->getClientOriginalName();
        $baiviet = new baiviet;
        $baiviet->title= $request->title;
        $baiviet->slug= str_slug($request->title);
        $baiviet->trichdan= $request->trichdan;
        $baiviet->noidung= $request->noidung;
        $baiviet->img= $filename;
        $baiviet->baiviet_cate= $request->cate;
     
        $baiviet->save();
        $request->img->storeAs('img',$filename);
        return redirect('admin/baiviet')->with('flash_message','Bạn Thêm thành công  !!');
        
    }

    public function getEditBaiViet($id){ 
        $data['baiviet'] = baiviet::find($id);
        $data['catelist'] = cate::all();
        return view('backend.edit_baiviet',$data);
    }

    public function postEditBaiViet(Request $request, $id){ 
        $baiviet = new baiviet;
        
        $arr['title'] = $request->title;
        $arr['slug'] = str_slug($request->title);
        $arr['trichdan'] = $request->trichdan;
        $arr['noidung'] = $request->noidung;
        $arr['baiviet_cate'] = $request->cate;
        if($request->hasFile('img')){
            $img = $request->img->getclientOriginalName();// lấy về cái tên file ảnh
            $arr['img'] = $img;//truyền vào mảng
            $request->img->storeAs('img',$img); // lưu vào trong file có tên img
        }
        $baiviet::where('id',$id)->update($arr);
        return redirect('admin/baiviet')->with('flash_message','Bạn sửa thành công !!'); 
    }

    public function getDeleteBaiViet($id){ 
        baiviet::destroy($id);
        return back()->with('flash_message','Bạn xóa thành công !!');
        
    }
     public function getview($id){ 
        $data['baiviet'] = baiviet::find($id);
       
        return view('backend.view',$data);
    }

}
