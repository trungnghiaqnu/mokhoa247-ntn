<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\danhgia;
use App\Http\Requests\addBanggiaRequest;

class DanhgiaController extends Controller
{
    public function getDanhGia(){

        $data['danhgia'] = danhgia::all();
        return view('backend.danhgia',$data);
    }
    public function postAddDanhGia(addBanggiaRequest $request){
        $filename = $request->img->getClientOriginalName();
        $danhgia = new danhgia;
        $danhgia->img = $filename;
        $danhgia->name= $request->name;
        $danhgia->noidung= $request->noidung;
        $danhgia->save();
        $request->img->storeAs('img',$filename);
        return redirect('admin/danhgia')->with(['flash_message'=>'Bạn thêm thành công']);
      

    }
    public function getEditDanhGia($id){
        $data['danhgia'] = danhgia::find($id);
       
        return view('backend.edit_danhgia',$data);

    }
    public function postEditDanhGia(Request $request, $id){
        $danhgia = new danhgia;
        $arr['name'] = $request->name;
        $arr['noidung'] = $request->noidung;
      
        if($request->hasFile('img')){
            $img = $request->img->getclientOriginalName();// lấy về cái tên file ảnh
            $arr['img'] = $img;//truyền vào mảng
            $request->img->storeAs('img',$img); // lưu vào trong file có tên img
        }
        $danhgia::where('id',$id)->update($arr);
        return redirect('admin/danhgia')->with('flash_message','Bạn sửa thành công !!');

    }
    public function getDeleteDanhGia($id){

        danhgia::destroy($id);
        return back()->with('flash_message','Bạn xóa thành công !!');
    }
}
