<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;// chuyển các biến qua lại giữa các controller
use Illuminate\Routing\Controller as BaseController;// thực hiện được các thằng router
use Illuminate\Foundation\Validation\ValidatesRequests;// kiểm tra dữ liệu
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;//thực hiện cấp quyền

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
