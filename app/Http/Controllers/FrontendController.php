<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\slide;
use App\Model\banggia;
use App\Model\cate;
use App\Model\baiviet;
use App\Model\danhgia;


class FrontendController extends Controller
{
    public function getHome()
    {
        $data['banggia'] = banggia::all();
        $data['danhgia'] = danhgia::all();
        return view('frontend.index',$data);
    }
    public function getDetailBaiViet($id){
        
        $data['catename'] = cate::where('cate_slug', $id)->first();
        if($data['catename']){
            $data['baiviet'] = baiviet::where('baiviet_cate',$data['catename']->cate_id)->get();
            return view('frontend.detail_danhmuc',$data);
        }else{
            $data['baiviet'] = baiviet::where('slug', $id)->firstOrFail();
            $data['lienquan'] =baiviet::where('baiviet_cate', $data['baiviet']->baiviet_cate)
            ->where('id','<>',$id)->get();

            return view('frontend.detail_baiviet',$data);  
       }
    }

}
