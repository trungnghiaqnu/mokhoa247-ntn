<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Model\cate;
use App\Model\slide;
use App\Model\footer;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $data['cate'] = cate::all();
        $data['slide'] = slide::all();
        $data['footer'] = footer::all();

        view()->share($data);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
