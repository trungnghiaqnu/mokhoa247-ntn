<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class banggia extends Model
{
    protected $table = 'banggias';
    protected $primaryKey ='id';
    protected $fillable = ['id','img'];
}
