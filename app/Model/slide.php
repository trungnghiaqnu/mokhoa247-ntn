<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class slide extends Model
{
    protected $table = 'sliders';
    protected $primaryKey ='id';
    protected $fillable = ['id','img'];
}
