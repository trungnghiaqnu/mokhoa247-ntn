<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class danhgia extends Model
{
    protected $table = 'danhgias';
    protected $primaryKey ='id';
    protected $fillable = ['id','img','name','noidung'];
}
