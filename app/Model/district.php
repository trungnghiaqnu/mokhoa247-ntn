<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class district extends Model
{
    protected $table = 'district';
    protected $primaryKey ='districtid';
    protected $guarded = ['name','type','location','provinceid'];
}
