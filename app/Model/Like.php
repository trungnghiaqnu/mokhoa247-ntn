<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
     protected $table = 'likes';
    protected $primaryKey ='id';
    protected $fillable = ['user_id','baiviet_id','email'];
}
