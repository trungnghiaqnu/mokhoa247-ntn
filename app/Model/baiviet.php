<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class baiviet extends Model
{
    protected $table = 'baiviets';
    protected $primaryKey ='id';
    protected $fillable = ['id','title','slug','trichdan','img','noidung','baiviet_cate'];
}
