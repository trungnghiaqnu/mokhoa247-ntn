<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class cate extends Model
{
    protected $table = 'cates';
    protected $primaryKey ='cate_id';
    protected $fillable = ['cate_id','cate_name','cate_slug'];
}
