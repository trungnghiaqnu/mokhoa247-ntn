<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class footer extends Model
{
    protected $table = 'footers';
    protected $primaryKey ='id';
    protected $fillable = ['id','phone','mail','gioithieu'];
}
